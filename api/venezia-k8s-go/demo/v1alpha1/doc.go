// +k8s:deepcopy-gen=package
// +groupName=demo.michaelvenezia.com

// Package v1alpha1 is the v1alpha1 version of the API.
package v1alpha1 // import "gitlab.com/mvenezia/demo-k8s-api-app/api/venezia-k8s-go/demo/v1alpha1"
