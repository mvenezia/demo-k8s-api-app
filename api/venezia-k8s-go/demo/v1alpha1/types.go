package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// User is a specification for a User resource
type User struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   UserSpec   `json:"spec"`
	Status UserStatus `json:"status"`
}

// UserSpec is the spec for a User resource
type UserSpec struct {
	Enabled        bool           `json:"enabled"`
	FirstName      string         `json:"firstName"`
	LastName       string         `json:"lastName"`
	Username       string         `json:"username"`
	UserID         string         `json:"userID"`
	Email          string         `json:"email"`
	Token          string         `json:"token"`
	RefreshToken   string         `json:"refreshToken"`
	Password       string         `json:"password"`
	AdditionalData AdditionalData `json:"additionalData"`
}

type AdditionalData map[string]string

// UserStatus is the status for a User resource
type UserStatus struct {
	LastLogin         metav1.Time `json:"lastLogin"`
	TokenRefresh      metav1.Time `json:"tokenRefresh"`
	TokenTTL          metav1.Time `json:"tokenTTL"`
	TokenRefreshState string      `json:"tokenRefreshState"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// UserList is a list of User resources
type UserList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []User `json:"items"`
}

func (in *AdditionalData) DeepCopy() *AdditionalData {
	if in == nil {
		return nil
	}
	out := make(AdditionalData)
	//casted := unstructured.Unstructured(*in)
	//deepCopy := casted.DeepCopy()
	//out.Object = deepCopy.Object
	for key, value := range *in {
		out[key] = value
	}
	return &out
}
