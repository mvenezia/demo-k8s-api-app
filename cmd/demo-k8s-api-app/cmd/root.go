package cmd

import (
	"flag"
	"fmt"
	"gitlab.com/mvenezia/demo-k8s-api-app/internal/demo"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	versioncmd "gitlab.com/mvenezia/demo-k8s-api-app/cmd/demo-k8s-api-app/cmd/version"
	"gitlab.com/mvenezia/demo-k8s-api-app/internal/constants"
)

var (
	rootCmd = &cobra.Command{
		Use:   "demo-k8s-api-app",
		Short: "Demo k8s-api application",
		Long:  `A demo of a k8s-api based application`,
		Run: func(cmd *cobra.Command, args []string) {
			err := runWebServer()
			if err != nil {
				fmt.Printf("%s\n", err)
			}
		},
	}
)

func init() {
	// Setup viper
	viper.SetEnvPrefix(constants.ProjectPrefix)
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)

	// Parse all the CLI Flags
	parseFlags()

	// Bind the flags up
	err := viper.BindPFlags(rootCmd.Flags())
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	viper.AutomaticEnv()

	// Add the known goflags
	rootCmd.Flags().AddGoFlagSet(flag.CommandLine)

	// add sub-commands - namely the version
	rootCmd.AddCommand(versioncmd.NewCommand())
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func parseFlags() {
	rootCmd.Flags().String(constants.K8SKubeconfigContents, constants.K8SKubeconfigContentsDefault, constants.K8SKubeconfigContentsDescription)
	rootCmd.Flags().String(constants.K8SKubeconfigLocation, constants.K8SKubeconfigLocationDefault, constants.K8SKubeconfigLocationDescription)
	rootCmd.Flags().String(constants.K8SNamespace, constants.K8SNamespaceDefault, constants.K8SNamespaceDescription)
	rootCmd.Flags().Bool(constants.K8SReadOnly, constants.K8SReadOnlyDefault, constants.K8SReadOnlyDescription)
	rootCmd.Flags().Int(constants.LogLevel, constants.LogLevelDefault, constants.LogLevelDescription)
	rootCmd.Flags().String(constants.LogOutput, constants.LogOutputDefault, constants.LogOutputDescription)
	rootCmd.Flags().String(constants.LogErrorOutput, constants.LogErrorOutputDefault, constants.LogErrorOutputDescription)
	rootCmd.Flags().String(constants.LogFormat, constants.LogFormatDefault, constants.LogFormatDescription)
}

func runWebServer() error {
	// Create the server and give it a config
	server, err := demo.New(&demo.Config{
		K8S: &demo.K8SConfig{
			KubeconfigContents: []byte(viper.GetString(constants.K8SKubeconfigContents)),
			KubeconfigLocation: viper.GetString(constants.K8SKubeconfigLocation),
			Namespace:          viper.GetString(constants.K8SNamespace),
			ReadOnly:           viper.GetBool(constants.K8SReadOnly),
		},
		Logging: &demo.LoggingConfig{
			Format:          viper.GetString(constants.LogFormat),
			Level:           viper.GetInt(constants.LogLevel),
			OutputPath:      viper.GetString(constants.LogOutput),
			ErrorOutputPath: viper.GetString(constants.LogErrorOutput),
		},
	})
	if err != nil {
		return err
	}
	return server.DoStuff()
}
