package constants

import "go.uber.org/zap/zapcore"

const (
	K8SKubeconfigLocation = "k8s-kubeconfig-location"
	K8SKubeconfigContents = "k8s-kubeconfig-contents"
	K8SNamespace          = "k8s-namespace"
	K8SReadOnly           = "k8s-readonly"
	LogLevel              = "log-level"
	LogOutput             = "log-output"
	LogErrorOutput        = "log-output-stderror"
	LogFormat             = "log-format"

	K8SKubeconfigLocationDefault = ""
	K8SKubeconfigContentsDefault = ""
	K8SNamespaceDefault          = "default"
	K8SReadOnlyDefault           = true
	LogLevelDefault              = int(zapcore.WarnLevel)
	LogOutputDefault             = "stdout"
	LogErrorOutputDefault        = "stdout"
	LogFormatDefault             = "json"

	K8SKubeconfigLocationDescription = "Absolute file location for a kubeconfig, blank if in cluster"
	K8SKubeconfigContentsDescription = "Contents of a kubeconfig, blank if in cluster"
	K8SNamespaceDescription          = "Kubernetes namespace to use"
	K8SReadOnlyDescription           = "If Kubernetes is in read-only mode"
	LogLevelDescription              = "What level of logging should we support, \n\t-1: Debug, \n\t 0: Info, \n\t 1: Warning \n\t 2: Error \n\t 3: Debug Panic, \n\t 4: Panic \n\t 5: Fatal\nSet to a number"
	LogOutputDescription             = "Where should non-error log messages be sent, stderr and stdout currently supported"
	LogErrorOutputDescription        = "Where should error log messages be sent, stderr and stdout currently supported"
	LogFormatDescription             = "Format of log output, currently only json and console are supported"

	ProjectPrefix = "MIKE_K8S_API_DEMO"
)
