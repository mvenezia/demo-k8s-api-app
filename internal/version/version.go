package version

import (
	"context"

	versioninfo "gitlab.com/mvenezia/version-info/pkg/version"
)

type Service interface {
	Version(context.Context) Version
}

type Version struct {
	GitVersion   string
	GitCommit    string
	GitTreeState string
	BuildDate    string
	GoVersion    string
	Compiler     string
	Platform     string
}

type service struct{}

func NewService() Service {
	return service{}
}

func (service) Version(context.Context) Version {
	v := versioninfo.Get()
	return Version{
		GitVersion:   v.GitVersion,
		GitCommit:    v.GitCommit,
		GitTreeState: v.GitTreeState,
		BuildDate:    v.BuildDate,
		GoVersion:    v.GoVersion,
		Compiler:     v.Compiler,
		Platform:     v.Platform,
	}
}
