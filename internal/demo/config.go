package demo

type Config struct {
	K8S     *K8SConfig
	Logging *LoggingConfig
}

type K8SConfig struct {
	KubeconfigContents []byte
	KubeconfigLocation string
	Namespace          string
	ReadOnly           bool
}

type LoggingConfig struct {
	Format          string
	Level           int
	OutputPath      string
	ErrorOutputPath string
}
