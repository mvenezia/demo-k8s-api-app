package demo

import (
	"gitlab.com/mvenezia/demo-k8s-api-app/internal/user"
)

// EnsureMaps is a housekeeping function - it will make sure that the store's maps are set up
func (d *Demo) EnsureMaps() {
	d.MapMutex.Lock()
	defer d.MapMutex.Unlock()

	// if there is no enabled plans map set yet, set it up
	if d.EnabledUsers == nil {
		d.EnabledUsers = make(map[string]bool)
	}
	// if there is no user map set yet, set it up
	if d.Users == nil {
		d.Users = make(map[string]*user.User)
	}
}

// enableUserMap will enable a user in the EnabledUsers map
func (d *Demo) enableUserMap(name string) {
	d.MapMutex.Lock()
	defer d.MapMutex.Unlock()
	d.EnabledUsers[name] = true
}

// disableUserMap will disable a user in the EnabledUsers map
func (d *Demo) disableUserMap(name string) {
	d.MapMutex.Lock()
	defer d.MapMutex.Unlock()
	d.EnabledUsers[name] = false
}

// deleteUserMap will delete a user in the various maps
func (d *Demo) deleteUserMap(name string) {
	d.MapMutex.Lock()
	defer d.MapMutex.Unlock()

	delete(d.EnabledUsers, name)
	delete(d.Users, name)
}
