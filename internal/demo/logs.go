package demo

import (
	"fmt"

	"go.uber.org/zap"
)

const (
	FormatConsole    = "console"
	FormatJSON       = "json"
	OutputPathStdErr = "stderr"
	OutputPathStdOut = "stdout"

	InvalidLevelError = "invalid log level set"

	invalidOutputPathError    = "invalid output path - acceptable values are "
	invalidErrOutputPathError = "invalid error output path - acceptable values are "
	invalidFormatPathError    = "invalid format - acceptable values are "
)

var (
	ErrInvalidLevel           = fmt.Errorf(InvalidLevelError)
	ErrInvalidOutputPath      = fmt.Errorf(invalidOutputPathError + OutputPathStdOut + ", " + OutputPathStdErr)
	ErrInvalidErrorOutputPath = fmt.Errorf(invalidErrOutputPathError + OutputPathStdOut + ", " + OutputPathStdErr)
	ErrInvalidFormat          = fmt.Errorf(invalidFormatPathError + FormatJSON + ", " + FormatConsole)

	logger *zap.Logger
)
