package demo

import (
	"context"
	"gitlab.com/mvenezia/demo-k8s-api-app/internal/k8s/michaelvenezia"
	"gitlab.com/mvenezia/demo-k8s-api-app/internal/user"
	"sync"
	"time"

	"go.uber.org/zap"
)

type Demo struct {
	Config *Config
	Logger *zap.Logger // Logger to be used for messages

	ctx         context.Context    // Context used within the store for requests, signaling, etc.
	ctxCancel   context.CancelFunc // Function that if invoked will cancel the store's context
	stopChannel chan struct{}      // Kubernetes client-go libraries require a channel to signal to stop - this is that channel

	EnabledUsers map[string]bool       // Map that indicates whether a user is enabled, indexed by userid
	Users        map[string]*user.User // Map that provides a user for a given userid
	MapMutex     sync.RWMutex          // Mutex to be used to lock the maps when reads or writes are being done
}

func (d *Demo) DoStuff() error {
	d.EnsureMaps()

	errorChannel := make(chan error)
	var mvClient michaelvenezia.Client

	mvClient = michaelvenezia.Client{
		KubeconfigLocation:  d.Config.K8S.KubeconfigLocation,
		Kubeconfig:          d.Config.K8S.KubeconfigContents,
		KubernetesNamespace: d.Config.K8S.Namespace,
		KubernetesReadOnly:  d.Config.K8S.ReadOnly,
		ParentContext:       context.Background(),
		Logger:              d.Logger,
		Users: michaelvenezia.UsersConfiguration{
			Workers:      1,
			EnabledUsers: &d.EnabledUsers,
			Users:        &d.Users,
			MapMutex:     &d.MapMutex,
		},
	}
	err := mvClient.InitializeK8s()
	if err != nil {
		errorChannel <- err
	}

	go func() {
		err = mvClient.StartControllers()
		if err != nil {
			errorChannel <- err
		}
	}()

	time.Sleep(5 * time.Second)

	userClient, err := mvClient.GetUserClient()
	if err != nil {
		return err
	}

	updateError := userClient.New(&user.User{
		Enabled:             true,
		FirstName:           "Michael",
		LastName:            "Venezia",
		Username:            "venezia",
		UserID:              "mvenezia-gmail-com",
		Email:               "mvenezia@gmail.com",
		Token:               "",
		Password:            "",
		RefreshToken:        "",
		AdditionalData:      make(map[string]string),
		AdditionalDataMutex: sync.RWMutex{},
	})
	if updateError != nil {
		d.Logger.Info("could not update user venezia", zap.Error(updateError))
	}

	select {
	case err = <-errorChannel:
		return err
	}

	return nil
}

func New(config *Config) (*Demo, error) {
	var err error
	// To begin with, let's set up logging...
	logger, err = initializeLogger(config.Logging)
	if err != nil {
		return nil, err
	}

	return &Demo{
		Config: config,
		Logger: logger,
	}, nil
}
