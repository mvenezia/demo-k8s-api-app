package michaelvenezia

import (
	"context"
	"io/ioutil"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	demov1alpha1 "gitlab.com/mvenezia/demo-k8s-api-app/internal/k8s/michaelvenezia/demo/v1alpha1"
	clientset "gitlab.com/mvenezia/demo-k8s-api-app/pkg/generated/k8s/michaelvenezia/clientset/versioned"
)

const (
	UserController = "user"
)

// InitializeK8s will start the Demo with regard to K8S.
//
// It will return an error in a couple of situations:
// * if the environment (configuration, etc.) isn't correct
// * if the controller encounters an error
//
// Start will block, so put it probably makes sense to invoke it in a goroutine
func (c *Client) InitializeK8s() error {
	var err error

	// Get the stop signals up and ready...
	c.stopChannel = make(chan struct{})

	// Create a kubernetes client if a client wasn't provided to us...
	if c.Client == nil {
		c.Client, err = c.CreateClient()
		if err != nil {
			return err
		}
	}

	// if we weren't provided a parent context we will just default to context.Background()
	if c.ParentContext == nil {
		c.ParentContext = context.Background()
	}
	// Set the local context (and its respective cancel function) based on the now-set parentContext
	c.ctx, c.ctxCancel = context.WithCancel(c.ParentContext)

	return nil
}

func (c *Client) StartControllers() error {
	// Now that we are about to start controllers, let's be ready to cancel if our parent context is done
	go c.waitForCancel()

	c.controllers = make(map[string]Controller)

	// We will have two controllers running, each could conceivably return an error, so let's give the buffer a size of 2
	errChannel := make(chan error, 2)

	// Let's start up the demo/User/v1alpha1 controller
	go func() {
		c.controllers[UserController] = &demov1alpha1.UserController{
			KubernetesNamespace: c.KubernetesNamespace,
			KubernetesReadOnly:  c.KubernetesReadOnly,
			Client:              c.Client,
			Workers:             c.Users.Workers,
			ParentContext:       c.ctx,
			Logger:              c.Logger,
			EnabledUsers:        c.Users.EnabledUsers,
			Users:               c.Users.Users,
			MapMutex:            c.Users.MapMutex,
		}
		// If an error occurs, submit it to the error channel
		controllerError := c.controllers[UserController].Start()
		if controllerError != nil {
			errChannel <- controllerError
		}
	}()

	// Now we will block until one of two things happen: an error is returned or our context is done
	for {
		select {
		// If an error occurs we will return the first error.
		case newError := <-errChannel:
			{
				c.ctxCancel()
				return newError
			}
		// If our context is done, we will exit gracefully (not return an error)
		case <-c.ctx.Done():
			return nil
		}
	}
}

func (c *Client) GetUserClient() (demov1alpha1.UserClient, error) {
	if c.controllers[UserController] == nil {
		return nil, &Error{
			Type:    ExecutionErrorType,
			Message: ControllerNotInitializedError,
		}
	}
	
	return c.controllers[UserController].(demov1alpha1.UserClient), nil
}

// CreateClient will try to create a kubernetes client
func (c *Client) CreateClient() (*clientset.Clientset, error) {
	config, err := c.GetConfig()
	if err != nil {
		return nil, err
	}

	return clientset.NewForConfig(config)
}

// GetConfig will try to get a working kubernetes client configuration returning an error if it cannot
func (c *Client) GetConfig() (*rest.Config, error) {
	var err error
	var config *rest.Config

	// Let's see if an explicit location for kubeconfig has been provided...
	if c.KubeconfigLocation != "" {
		c.Kubeconfig, err = ioutil.ReadFile(c.KubeconfigLocation)
		if err != nil {
			return nil, &Error{
				Type:    InputErrorType,
				Message: LogCannotReadKubeconfigFile,
				Err:     err,
			}
		}
	}

	// Let's see if we now have the raw contents of a kubeconfig...
	if len(c.Kubeconfig) > 0 {
		config, err = clientcmd.RESTConfigFromKubeConfig(c.Kubeconfig)
		if err != nil {
			return nil, &Error{
				Type:    InputErrorType,
				Message: LogCannotConvertKubeconfigContents,
				Err:     err,
			}
		}
	} else {
		// No explicit kubeconfig was specified, let's see if we're in a cluster already...
		config, err = rest.InClusterConfig()
		if err == rest.ErrNotInCluster {
			return nil, &Error{
				Type:    InputErrorType,
				Message: LogCannotUseInClusterConfig,
				Err:     err,
			}
		}
		if err != nil {
			return nil, &Error{
				Type:    ExecutionErrorType,
				Message: LogCannotConvertInClusterConfig,
				Err:     err,
			}
		}
	}

	return config, nil
}

// waitForCancel will signal to the internal system (the demo controller) to shut down
func (c *Client) waitForCancel() {
	for {
		<-c.ctx.Done()
		close(c.stopChannel)
		return
	}
}

// Shutdown will shut the demo down.
// This is a helper function in that the context (and its cancel function) are locally scoped, so an outsider
// can call this function and shut the store down
func (c *Client) Shutdown() {
	c.ctxCancel()
}
