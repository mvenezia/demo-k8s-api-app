package michaelvenezia

import (
	"context"
	"gitlab.com/mvenezia/demo-k8s-api-app/internal/user"
	clientset "gitlab.com/mvenezia/demo-k8s-api-app/pkg/generated/k8s/michaelvenezia/clientset/versioned"
	"go.uber.org/zap"
	"sync"
)

type Client struct {
	KubeconfigLocation  string // Where a kubeconfig file is located on the system
	Kubeconfig          []byte // The contents of a kubeconfig
	KubernetesNamespace string // The namespace that will be used to look for resources
	KubernetesReadOnly  bool   // Whether we are in read-only mode

	Client        *clientset.Clientset // A Kubernetes client used in the controller
	ParentContext context.Context      // If provided, upstream cancellations of context will cascade down
	Logger        *zap.Logger          // Logger to be used for messages

	ctx         context.Context    // Context used within the store for requests, signaling, etc.
	ctxCancel   context.CancelFunc // Function that if invoked will cancel the store's context
	stopChannel chan struct{}      // Kubernetes client-go libraries require a channel to signal to stop - this is that channel

	controllers map[string]Controller // Map of controllers that this package has

	Users UsersConfiguration
}

type UsersConfiguration struct {
	Workers      int
	EnabledUsers *map[string]bool       // Map that indicates whether a user is enabled, indexed by userid
	Users        *map[string]*user.User // Map that provides a user for a given userid
	MapMutex     *sync.RWMutex
}

type Controller interface {
	Start() error
}
