package v1alpha1

const (
	// User Controller Log Messages
	LogUserControllerDeleteFunctionTrigger               = "user controller delete event handler triggered"
	LogUserControllerAddFunctionTrigger                  = "user controller add event handler triggered"
	LogUserControllerUpdateFunctionTrigger               = "user controller update event handler triggered"
	LogUserControllerUpdateFunctionFailedTypecastOldUser = "cannot typecast presumed old user into a user object"
	LogUserControllerUpdateFunctionFailedTypecastNewUser = "cannot typecast presumed new user into a user object"
	LogFailedWaitingUserInformerCacheSync                = "failed waiting for user informer caches to sync"
	LogStartingUserController                            = "starting user controller"
	LogStartedUserWorkers                                = "started user controller workers"
	LogStartingUserWorkers                               = "starting user controller workers"
	LogStoppingUserWorkers                               = "stopping user controller workers"
	LogWaitingUserInformerCacheSync                      = "waiting for user informer caches to sync"
	LogAddingUserCacheKeyToWorkQueue                     = "adding user cache key into workqueue"
	LogSynchedUserCacheItem                              = "successfully synched user cache key into data stores"
	LogUserControllerDeleteUser                          = "user was deleted, updating maps"
	LogUserControllerAddUser                             = "user was added, updating maps"
	LogUserControllerUpdateSameResource                  = "user in cache is already the same resource version, ignoring"
	LogUserControllerUpdateSyncNeeded                    = "user in cache is different from new resource version, will need sync"
	LogUserInvalidResourceKey                            = "invalid cache key was detected in the user cache"
	LogErrorEnqueueingUserObject                         = "error occurred while enqueueing a user object"
	LogErrorProcessingUserObject                         = "error occurred while processing a user object"
	LogUserTypeErrorSyncingUser                          = "error occurred while trying to sync user, will retry"
	LogUserCacheKeyStringAssertionFailed                 = "expected a string value for user cache key, got something else"
	LogUserControllerAddUserAlreadyExists                = "user was in theory updated"
	LogUserControllerMarkingUserEnabledInMaps            = "marking user enabled in local maps"
	LogUserControllerMarkingUserDisabledInMaps           = "marking user disabled in local maps"

	// Log Keys
	LogUserKey                 = "user"
	LogKubernetesCacheKey      = "k8sCacheKey"
	LogKubernetesCacheValueKey = "k8sCacheValueKey"
	LogUserValueKey            = "userValue"
)
