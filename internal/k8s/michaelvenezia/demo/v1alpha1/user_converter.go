package v1alpha1

import (
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sync"

	k8s "gitlab.com/mvenezia/demo-k8s-api-app/api/venezia-k8s-go/demo/v1alpha1"
	"gitlab.com/mvenezia/demo-k8s-api-app/internal/user"
)

// ConvertUserToKubernetes will convert a user from our internal representation - User to a kubernetes v1alpha1.User object
// This is done so that we do not try to tightly bind our business logic to how we express it through various APIs
func ConvertUserToKubernetes(input *user.User) *k8s.User {
	input.AdditionalDataMutex.RLock()
	defer input.AdditionalDataMutex.RUnlock()

	//additionalData := k8s.AdditionalData{Object: make(map[string]interface{})}
	//for key, value := range input.AdditionalData {
	//	additionalData.Object[key] = value
	//}
	additionalData := make(k8s.AdditionalData)
	for key, value := range input.AdditionalData {
		additionalData[key] = value
	}
	return &k8s.User{
		ObjectMeta: v1.ObjectMeta{
			Name: input.UserID,
		},
		Spec: k8s.UserSpec{
			Enabled:        input.Enabled,
			FirstName:      input.FirstName,
			LastName:       input.LastName,
			Username:       input.Username,
			UserID:         input.UserID,
			Email:          input.Email,
			Token:          input.Token,
			RefreshToken:   input.RefreshToken,
			Password:       input.Password,
			AdditionalData: additionalData,
		},
	}
}

// ConvertUserFromV1Alpha1Kubernetes will convert a user from the k8s representation - v1alpha1.User to our internal represenation
// This is done so that we do not try to tightly bind our business logic to how we express it through various APIs
func ConvertUserFromV1Alpha1Kubernetes(input *k8s.User) *user.User {
	additionalData := make(map[string]string)
	//for key, value := range input.Spec.AdditionalData.Object {
	for key, value := range input.Spec.AdditionalData {
		additionalData[key] = value
	}
	return &user.User{
		Enabled:             input.Spec.Enabled,
		FirstName:           input.Spec.FirstName,
		LastName:            input.Spec.LastName,
		Username:            input.Spec.Username,
		UserID:              input.GetName(),
		Email:               input.Spec.Email,
		Token:               input.Spec.Token,
		RefreshToken:        input.Spec.RefreshToken,
		Password:            input.Spec.Password,
		AdditionalData:      additionalData,
		AdditionalDataMutex: sync.RWMutex{},
	}
}
