package v1alpha1

import (
	"context"
	"sync"
	"time"

	"go.uber.org/zap"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"

	"gitlab.com/mvenezia/demo-k8s-api-app/internal/user"
	clientset "gitlab.com/mvenezia/demo-k8s-api-app/pkg/generated/k8s/michaelvenezia/clientset/versioned"
	listers "gitlab.com/mvenezia/demo-k8s-api-app/pkg/generated/k8s/michaelvenezia/listers/demo/v1alpha1"
)

const (
	// InformerDefaultResyncTimer is used to decide when to do a full sync with the kubernetes API
	InformerDefaultResyncTimer = time.Minute * 30
)

type UserController struct {
	KubernetesNamespace string // The namespace that will be used to look for resources
	KubernetesReadOnly  bool   // Whether we are in read-only mode

	Client        *clientset.Clientset // A Kubernetes client used in the controller
	Workers       int                  // The number of workers for the user controller (to sync state)
	ParentContext context.Context      // If provided, upstream cancellations of context will cascade down
	Logger        *zap.Logger          // Logger to be used for messages

	ctx         context.Context    // Context used within the store for requests, signaling, etc.
	ctxCancel   context.CancelFunc // Function that if invoked will cancel the store's context
	stopChannel chan struct{}      // Kubernetes client-go libraries require a channel to signal to stop - this is that channel

	Lister    listers.UserLister              // The user controller's user lister
	Synced    cache.InformerSynced            // The user controller's indicator if the informer has synced
	Workqueue workqueue.RateLimitingInterface // The work queue for the user controller

	EnabledUsers *map[string]bool       // Map that indicates whether a user is enabled, indexed by userid
	Users        *map[string]*user.User // Map that provides a user for a given userid
	MapMutex     *sync.RWMutex
}
