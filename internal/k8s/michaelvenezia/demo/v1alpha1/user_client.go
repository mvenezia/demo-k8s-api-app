package v1alpha1

import (
	"gitlab.com/mvenezia/demo-k8s-api-app/internal/user"
)

type UserClient interface {
	Delete(user string) error
	New(user *user.User) error
	Update(user *user.User) error
}
