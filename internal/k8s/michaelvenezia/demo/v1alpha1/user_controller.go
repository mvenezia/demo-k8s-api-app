package v1alpha1

import (
	"context"
	"fmt"
	"time"

	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"

	"gitlab.com/mvenezia/demo-k8s-api-app/api/venezia-k8s-go/demo/v1alpha1"
	"gitlab.com/mvenezia/demo-k8s-api-app/internal/user"
	informers "gitlab.com/mvenezia/demo-k8s-api-app/pkg/generated/k8s/michaelvenezia/informers/externalversions"
)

const (
	UserWorkqueueName = "Users" // What do we want the work queue to be named
)

// Start starts the user controller
// It will return an error if it cannot start an informer factory or if the informer can never sync itself
func (u *UserController) Start() error {
	var err error
	u.ctx, u.ctxCancel = context.WithCancel(u.ParentContext)

	// Let's start our user informer factory, scoped to the namespace that we're configured with and the resync timer
	userInformerFactory := informers.NewSharedInformerFactoryWithOptions(u.Client, InformerDefaultResyncTimer, informers.WithNamespace(u.KubernetesNamespace))

	// Now that we have our factory let's create our informer
	userInformer := userInformerFactory.Demo().V1alpha1().Users()

	// Now that we have our informer we can create our lister and our indicator function if the informer has gotten a full list
	u.Lister = userInformer.Lister()
	u.Synced = userInformer.Informer().HasSynced

	// Let's also create our work queue using the default rate limiter
	u.Workqueue = workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), UserWorkqueueName)

	// Now that we have our informer, let's add an event handler that will queue add/update/delete events to the work queue
	userInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    u.UsersAddFunc,
		UpdateFunc: u.UsersUpdateFunc,
		DeleteFunc: u.UsersDeleteFunc,
	})

	// Now that we are "set", let's start the factory, using our store's stop channel as a signal to stop
	userInformerFactory.Start(u.stopChannel)

	// Now that factory has started, we can start up the actual "controller" using our store's stop channel as a signal to stop
	// This will block until the stop channel is closed or an error is returned
	err = u.RunUserController(u.stopChannel)
	if err != nil {
		// If an error occurs, we should return it upstream
		return err
	}

	// No error occurred so let's gracefully exit
	return nil
}

// enqueueUser will take an object, use the kubernetes cache's function to generate a key name from it, and stuff
// the resulting key name into the user's work queue
func (u *UserController) enqueueUser(obj interface{}) {
	var key string
	var err error

	// Have the cache's preferred key name generator generate a name...
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		// Can't generate a name ... so we can't really do much at this point but log an error and hope a resync
		// will resolve it later...
		u.Logger.Warn(LogErrorEnqueueingUserObject, zap.Error(err))
		return
	}

	// For debugging purposes, log that we did in fact add the key to the user work queue
	u.Logger.Debug(LogAddingUserCacheKeyToWorkQueue, zap.String(LogKubernetesCacheKey, key))
	u.Workqueue.Add(key)
}

// processNextUserItem will read a single work item off the usersWorkqueue and attempt to process it, by
// calling the userSyncHandler.
func (u *UserController) processNextUserItem() bool {
	// Get the next user to sync our internal representation to.  This will block until one of two things happen:
	// * a user is ready to be processed
	// * shutdown is happening (s.stopChannel was closed elsewhere)
	obj, shutdown := u.Workqueue.Get()

	// If we're in shutdown mode, no need to do anything, just bail...
	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.usersWorkqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the usersWorkqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the usersWorkqueue and attempted again after a back-off
		// period.
		defer u.Workqueue.Done(obj)

		var key string
		var ok bool

		// We expect strings to come off the usersWorkqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// usersWorkqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// usersWorkqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the usersWorkqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			u.Workqueue.Forget(obj)
			u.Logger.Warn(LogUserCacheKeyStringAssertionFailed, zap.Any(LogKubernetesCacheValueKey, obj))
			return nil
		}

		// RunUserController the userSyncHandler, passing it the namespace/name string of the
		// User resource to be synced.
		if err := u.userSyncHandler(key); err != nil {
			// Put the item back on the usersWorkqueue to handle any transient errors.
			u.Workqueue.AddRateLimited(key)
			u.Logger.Warn(LogUserTypeErrorSyncingUser, zap.String(LogKubernetesCacheKey, key), zap.Error(err))
			return &Error{
				Type:    TransientErrorType,
				Message: LogUserTypeErrorSyncingUser,
			}
		}

		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		u.Workqueue.Forget(obj)
		u.Logger.Debug(LogSynchedUserCacheItem, zap.String(LogKubernetesCacheKey, key))
		return nil
	}(obj)

	// An error occurred while processing the item, let's log the error...
	if err != nil {
		u.Logger.Warn(LogErrorProcessingUserObject, zap.Error(err))
	}

	return true
}

// userSyncHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the User resource
// with the current status of the resource.
func (u *UserController) userSyncHandler(key string) error {
	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		u.Logger.Warn(LogUserInvalidResourceKey, zap.String(LogKubernetesCacheKey, key))
		return nil
	}

	// Get the User resource with this namespace/name
	userValue, err := u.Lister.Users(namespace).Get(name)
	if err != nil {
		// The User resource may no longer exist, in which case we mark it as deleted
		if errors.IsNotFound(err) {
			u.Logger.Info(LogUserControllerDeleteUser, zap.String(LogUserKey, name))
			u.deleteUserMap(name)
			return nil
		}

		return err
	}

	// We're going to mutate the maps, so we need to get a lock on them...
	u.MapMutex.Lock()

	// Let's see if the user already exists in our map...
	_, ok := (*u.Users)[name]
	if !ok {
		// We need to insert this user into the mix...
		convertedUser := ConvertUserFromV1Alpha1Kubernetes(userValue)
		u.Logger.Info(LogUserControllerAddUser, zap.String(LogUserKey, name), zap.Any(LogUserValueKey, convertedUser))
		(*u.Users)[name] = convertedUser
	} else {
		// User already exists, so going to overwrite the map entry...
		convertedUser := ConvertUserFromV1Alpha1Kubernetes(userValue)
		u.Logger.Info(LogUserControllerAddUserAlreadyExists, zap.String(LogUserKey, name), zap.Any(LogUserValueKey, convertedUser))
		(*u.Users)[name] = convertedUser
	}

	// Done mutating, so unlock the maps
	u.MapMutex.Unlock()

	// One change that we will consider is whether the user is enabled or not.  Here we will have a function
	// set the user as enabled/disabled
	if userValue.Spec.Enabled {
		u.Logger.Info(LogUserControllerMarkingUserEnabledInMaps, zap.String(LogUserKey, name))
		u.enableUserMap(name)
	} else {
		u.Logger.Info(LogUserControllerMarkingUserDisabledInMaps, zap.String(LogUserKey, name))
		u.disableUserMap(name)
	}

	// We are done, exit gracefully...
	return nil
}

// RunUserController will set up the event handlers for types we are interested in, as well
// as syncing informer caches and starting workers. It will block until stopCh
// is closed, at which point it will shutdown the usersWorkqueue and wait for
// workers to finish processing their current work items.
func (u *UserController) RunUserController(stopCh <-chan struct{}) error {
	defer u.Workqueue.ShutDown()

	// Start the informer factories to begin populating the informer caches
	u.Logger.Info(LogStartingUserController)

	// Wait for the caches to be synced before starting workers
	u.Logger.Info(LogWaitingUserInformerCacheSync)
	if ok := cache.WaitForCacheSync(stopCh, u.Synced); !ok {
		return fmt.Errorf(LogFailedWaitingUserInformerCacheSync)
	}

	u.Logger.Info(LogStartingUserWorkers)
	// Launch workers to process User resources
	for i := 0; i < u.Workers; i++ {
		go wait.Until(u.runUserWorker, time.Second, stopCh)
	}

	u.Logger.Info(LogStartedUserWorkers)
	// Block until the stop channel has closed...
	<-stopCh
	u.Logger.Info(LogStoppingUserWorkers)

	return nil
}

// runUserWorker is a long-running function that will continually call the
// processNextUserItem function in order to read and process a message on the
// usersWorkqueue.
func (u *UserController) runUserWorker() {
	for u.processNextUserItem() {
		// Start the informer factories to begin populating the informer caches
	}
}

// UsersAddFunc is called when the informer sees that a user has been added to the mix (from within kubernetes)
// We will simply add the user to the work queue to be processed
func (u *UserController) UsersAddFunc(newObject interface{}) {
	u.Logger.Info(LogUserControllerAddFunctionTrigger)
	u.enqueueUser(newObject)
}

// UsersUpdateFunc is called when the informer sees that a user has been updated (from within kubernetes)
// We will check to see if we've seen this resource version already (thus it would indicate a noop) add if
// there is a new resource version we will add the user to the work queue to be processed
func (u *UserController) UsersUpdateFunc(oldObject interface{}, newObject interface{}) {
	var oldUser, newUser *v1alpha1.User
	var ok bool
	u.Logger.Info(LogUserControllerUpdateFunctionTrigger)

	if oldUser, ok = oldObject.(*v1alpha1.User); !ok {
		u.Logger.Warn(LogUserControllerUpdateFunctionFailedTypecastOldUser)
		return
	}
	if newUser, ok = newObject.(*v1alpha1.User); !ok {
		u.Logger.Warn(LogUserControllerUpdateFunctionFailedTypecastNewUser)
		return
	}

	if oldUser.ResourceVersion == newUser.ResourceVersion {
		u.Logger.Debug(LogUserControllerUpdateSameResource, zap.String(LogUserKey, oldUser.GetName()))
	} else {
		u.Logger.Debug(LogUserControllerUpdateSyncNeeded, zap.String(LogUserKey, oldUser.GetName()))
		u.enqueueUser(newObject)
	}
}

// UsersDeleteFunc is called when the informer sees that a user has been removed from the mix (from within kubernetes)
// We will simply add the user to the work queue to be processed
func (u *UserController) UsersDeleteFunc(newObject interface{}) {
	u.Logger.Info(LogUserControllerDeleteFunctionTrigger)
	u.enqueueUser(newObject)
}

// New will submit a new user to kubernetes on behalf of a requester  This is going to be called by Add
func (u *UserController) New(user *user.User) error {
	kubernetesUser := ConvertUserToKubernetes(user)

	_, err := u.Client.DemoV1alpha1().Users(u.KubernetesNamespace).Create(u.ctx, kubernetesUser, v1.CreateOptions{})
	if err == nil {
		return nil
	}

	if errors.IsAlreadyExists(err) {
		return &Error{
			Type:    InputErrorType,
			Message: CannotAddSameUserNameInputError,
			Details: fmt.Sprintf("userid was named -->%s<--", user.UserID),
		}
	}

	return &Error{
		Type:    ExecutionErrorType,
		Message: UpstreamGeneralExecutionError,
		Err:     err,
	}
}

// Delete will ask kubernetes to delete a user on behalf of a requester  This is going to be called by Delete
func (u *UserController) Delete(user string) error {
	err := u.Client.DemoV1alpha1().Users(u.KubernetesNamespace).Delete(u.ctx, user, v1.DeleteOptions{})

	if err == nil {
		return nil
	}

	if errors.IsNotFound(err) {
		return &Error{
			Type:    InputErrorType,
			Message: UserDoesNotExistInputError,
			Details: fmt.Sprintf("user was named -->%s<--", user),
		}
	}

	return &Error{
		Type:    ExecutionErrorType,
		Message: UpstreamGeneralExecutionError,
		Err:     err,
	}
}

// Update will ask kubernetes to adjust the spec of an existing user.
func (u *UserController) Update(user *user.User) error {
	newUser, err := u.Lister.Users(u.KubernetesNamespace).Get(user.UserID)
	if err != nil {
		if errors.IsNotFound(err) {
			return &Error{
				Type:    InputErrorType,
				Message: UserDoesNotExistInputError,
				Details: fmt.Sprintf("user was named -->%s<--", user.Username),
			}
		}

		return &Error{
			Type:    ExecutionErrorType,
			Message: UpstreamGeneralExecutionError,
			Err:     err,
		}
	}

	// Mark the user as requested
	newUser.Spec = ConvertUserToKubernetes(user).Spec

	// Submit the updated user to kubernetes
	_, err = u.Client.DemoV1alpha1().Users(u.KubernetesNamespace).Update(u.ctx, newUser, v1.UpdateOptions{})
	if err == nil {
		return nil
	}

	if errors.IsNotFound(err) {
		return &Error{
			Type:    InputErrorType,
			Message: UserDoesNotExistInputError,
			Details: fmt.Sprintf("user was named -->%s<--", user.Username),
		}
	}

	return &Error{
		Type:    ExecutionErrorType,
		Message: UpstreamGeneralExecutionError,
		Err:     err,
	}
}

// enableUserMap will enable a user in the EnabledUsers map
func (u *UserController) enableUserMap(name string) {
	(*u.MapMutex).Lock()
	defer (*u.MapMutex).Unlock()
	(*u.EnabledUsers)[name] = true
}

// disableUserMap will disable a user in the EnabledUsers map
func (u *UserController) disableUserMap(name string) {
	(*u.MapMutex).Lock()
	defer (*u.MapMutex).Unlock()
	(*u.EnabledUsers)[name] = false
}

// deleteUserMap will delete a user in the various maps
func (u *UserController) deleteUserMap(name string) {
	(*u.MapMutex).Lock()
	defer (*u.MapMutex).Unlock()

	delete(*u.EnabledUsers, name)
	delete(*u.Users, name)
}
