package michaelvenezia

const (
	// Main Kubernetes Driver Messages
	LogCannotReadKubeconfigFile        = "cannot read in kubeconfig file as specified"
	LogCannotConvertKubeconfigContents = "could not convert kubeconfig contents to a valid configuration"
	LogCannotUseInClusterConfig        = "seemingly not in a kubernetes cluster, cannot use in cluster configuration"
	LogCannotConvertInClusterConfig    = "seemingly in a kubernetes cluster but cannot create a valid cluster configuration"
	LogStoreContextClosed              = "store context is closed, shutting down..."
	LogStoreShutdownCalled             = "store shutdown called, shutting down"
)
