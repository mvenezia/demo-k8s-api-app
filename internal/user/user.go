package user

import "sync"

type User struct {
	Enabled             bool
	FirstName           string
	LastName            string
	Username            string
	UserID              string
	Email               string
	Token               string
	Password            string
	RefreshToken        string
	AdditionalData      map[string]string
	AdditionalDataMutex sync.RWMutex
}
