ARG GOLANG_BASEIMAGE="golang:1.20.5-alpine3.18"
ARG ALPINE_BASEIMAGE="alpine:3.18"

# Initial preparations
FROM $GOLANG_BASEIMAGE
RUN /sbin/apk update && /sbin/apk add git make curl bash

# Let's add the code in
ADD . /go/src/gitlab.com/mvenezia/demo-k8s-api-app

# Build package in container
WORKDIR /go/src/gitlab.com/mvenezia/demo-k8s-api-app
RUN /bin/bash -c "make -f build/Makefile demo-k8s-api-app"

# Now that we have build a static binary, we can stuff it in a bare-bones alpine image
FROM $ALPINE_BASEIMAGE
COPY --from=0 /go/src/gitlab.com/mvenezia/demo-k8s-api-app/demo-k8s-api-app-linux-amd64 /demo-k8s-api-app

ENTRYPOINT ["/demo-k8s-api-app"]
