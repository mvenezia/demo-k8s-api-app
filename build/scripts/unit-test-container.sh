#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

DIRECTORIES="./internal/... ./cmd/..."
THIS_DIRECTORY=$(dirname "${BASH_SOURCE}")
PROJECT_DIRECTORY=${THIS_DIRECTORY}/../..

echo
echo "running unit tests ..."
echo "adding cobertura"
export CGO_ENABLED=1
GO111MODULE=off go get github.com/boumenot/gocover-cobertura
echo "go test ${DIRECTORIES} -coverprofile=coverage.txt"
go test ${DIRECTORIES} -coverprofile=coverage.txt
gocover-cobertura < coverage.txt > cobertura-coverage.xml

