#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

THIS_DIRECTORY=$(dirname "${BASH_SOURCE}")
PROJECT_DIRECTORY=${PWD}/${THIS_DIRECTORY}/../..

# URLPublishing URIBL v1 stuff
URLPUBLISHING_URIBL_V1_PROTOBUF_DIRECTORY_API=${PROJECT_DIRECTORY}/api/urlpublishing/uribl/v1

# URLPublishing URIBLAdmin v1 stuff
URLPUBLISHING_URIBLADMIN_V1_PROTOBUF_DIRECTORY_API=${PROJECT_DIRECTORY}/api/urlpublishing/uribladmin/v1

# Version v1 stuff
VERSION_V1_PROTOBUF_DIRECTORY_API=${PROJECT_DIRECTORY}/api/version/v1

DOCKER_ARGUMENTS="run --rm=true -it -w /code"
DOCKER_IMAGE=quay.io/venezia/protolock:v0.16.0-1

echo
echo "having protolock commit changes..."
echo "running docker ${DOCKER_ARGUMENTS} -v ${URLPUBLISHING_URIBL_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status"
docker ${DOCKER_ARGUMENTS} -v ${URLPUBLISHING_URIBL_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status
echo "running docker ${DOCKER_ARGUMENTS} -v ${URLPUBLISHING_URIBLADMIN_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status"
docker ${DOCKER_ARGUMENTS} -v ${URLPUBLISHING_URIBLADMIN_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status
echo "running docker ${DOCKER_ARGUMENTS} -v ${VERSION_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status"
docker ${DOCKER_ARGUMENTS} -v ${VERSION_V1_PROTOBUF_DIRECTORY_API}:/code ${DOCKER_IMAGE} status
