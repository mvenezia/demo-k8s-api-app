#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

THIS_DIRECTORY=$(dirname "${BASH_SOURCE}")
PROJECT_DIRECTORY=${THIS_DIRECTORY}/../..
GOLANGCI_IMAGE=quay.io/venezia/golang:1.20.3-tests

echo
echo "running linter ..."
echo "docker run --rm -v ${PWD}/${PROJECT_DIRECTORY}:/app -w /app ${GOLANGCI_IMAGE} ./build/scripts/golangci-lint-container.sh"
docker run --rm -v ${PWD}/${PROJECT_DIRECTORY}:/app -w /app ${GOLANGCI_IMAGE} ./build/scripts/golangci-lint-container.sh
