#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

THIS_DIRECTORY=$(dirname "${BASH_SOURCE}")
PROJECT_DIRECTORY=${THIS_DIRECTORY}/../..
GOLANGCI_IMAGE=golangci/golangci-lint:v1.50.1

echo
echo "running linter ..."
echo "golangci-lint run -v"
golangci-lint run -v
