#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

THIS_DIRECTORY=$(dirname "${BASH_SOURCE}")

PROJECT_DIRECTORY=${THIS_DIRECTORY}/../..

K8S_DESTINATION_LOCATION_API=${PROJECT_DIRECTORY}/pkg/generated/k8s

DOCKER_ARGUMENTS="run --rm=true --entrypoint /bin/bash -it -v ${PWD}:/go/src/gitlab.com/mvenezia/demo-k8s-api-app -w /go/src/gitlab.com/mvenezia/demo-k8s-api-app"
DOCKER_IMAGE=quay.io/venezia/k8s-golang-code-generator:1.27.3

echo "Ensuring Astro API Destination Directory ( ${K8S_DESTINATION_LOCATION_API} ) Exists..."
mkdir -p ${K8S_DESTINATION_LOCATION_API}

echo docker $DOCKER_ARGUMENTS $DOCKER_IMAGE \
       /go/code-generator/generate-groups.sh \
       "all" \
       gitlab.com/mvenezia/demo-k8s-api-app/pkg/generated/k8s/michaelvenezia \
       gitlab.com/mvenezia/demo-k8s-api-app/api/venezia-k8s-go \
       demo:v1alpha1 \
       --output-base "/go/src" \
       --go-header-file assets/k8s/boilerplate.txt

docker $DOCKER_ARGUMENTS $DOCKER_IMAGE \
       /go/code-generator/generate-groups.sh \
       "all" \
       gitlab.com/mvenezia/demo-k8s-api-app/pkg/generated/k8s/michaelvenezia \
       gitlab.com/mvenezia/demo-k8s-api-app/api/venezia-k8s-go \
       demo:v1alpha1 \
       --output-base "/go/src" \
       --go-header-file assets/k8s/boilerplate.txt

# docker run --rm=true -it --entrypoint /bin/bash -v $PWD:/go/src/gitlab.com/mvenezia/demo-k8s-api-app -w /go/src/hq-stash.corp.proofpoint.com/voltron/astro-backend 157e674318cd17022cabcc2e11964a4c02fa4aaf7bc522f127b5a621160f618b
# /go/code-generator/generate-groups.sh "deepcopy,client,informer,lister" hq-stash.corp.proofpoint.com/voltron/astro-backend/pkg/generated/k8s hq-stash.corp.proofpoint.com/voltron/astro-backend/api/voltron-k8s-go astro:v1alpha1 --go-header-file assets/k8s/boilerplate.txt